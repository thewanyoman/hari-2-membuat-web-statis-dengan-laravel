<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('page.register');
    }

    public function back(){
        return view('welcome');
    }
}
