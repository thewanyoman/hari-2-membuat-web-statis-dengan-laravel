<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function login(Request $request)
    {
        $first = $request->pertama;
        $last =  $request->terakhir;
        $gender = $request->cowok;
        $gender = $request->cewek;
        $national = $request->nationaly;
        $language1 = $request->indo;
        $language2 = $request->english;
        $language3 = $request->other;
        $bio = $request->bio;
        //dd($first, $last, $gender1, $gender2, $national, $language1, $language2, $language3, $bio);

        return view('page/home',compact('first', 'last', 'gender', 'language1', 'language2','language3', 'bio'));//lempar semua variable kelas ke home 
        
    }
}
